#include <iostream>
#include <functional>
#include <string>
#include <chrono>
#include <fstream>
#include <random>
using namespace std;
using namespace std::chrono;

void Swap(int& a, int& b)
{
	int temp = a;
	a = b;
	b = temp;
}

void BubbleSort(int numbers[], int N)
{
	// Moves things from the beginning to the end, depending on the values

	/*
	REPEAT N-1 times
		FOR each pair
			IF left > right
				Swap
			ENDIF
		ENDFOR
	ENDREPEAT
	*/

	bool breakEarly;
	int p;
	for (int i = 0; i < N - 1; ++i)
	{
		breakEarly = true;
		for (p = 0; p < N - 1 - i; ++p)
		{
			if (numbers[p] > numbers[p + 1])
			{
				Swap(numbers[p], numbers[p + 1]);
				breakEarly = false;
			}
		}
		if (breakEarly)
			break;
	}

}

void InsertionSort(int numbers[], int N)
{
	// March through the entire list once, 
	// finding the spot each element belongs to in the left (sorted) side.

	/*
	SET pointer to second item
	REPEAT until unsorted section is empty
		SELECT the first item in the unsorted section
		REPEAT until current item is larger than next sorted (left) item
			COMPARE current item with next sorted item
			MOVE sorted item to the right if it is bigger
		ENDREPEAT
		INSERT current item into this spot in sorted section
		ADVANCE the overall pointer
	ENDREPEAT
	*/

	for (int pointer = 1; pointer < N; ++pointer)
	{
		int current = numbers[pointer];
		for (int nextLeftSortedIndex = pointer - 1; nextLeftSortedIndex >= 0; --nextLeftSortedIndex)
		{
			if (current < numbers[nextLeftSortedIndex])
			{
				Swap(numbers[nextLeftSortedIndex], numbers[nextLeftSortedIndex + 1]);
			}
		}
	}
}

int FindMinimum(int* numbers, int count)
{
	//int minSoFar = numeric_limits<int>::max();
	if (count <= 0)
		return -1;

	int minIndexSoFar = 0;
	for (int i = 1; i < count; ++i)
	{
		if (numbers[minIndexSoFar] > numbers[i])
			minIndexSoFar = i;
	}

	return minIndexSoFar;
}

void SelectionSort(int numbers[], int count)
{
	/*
	REPEAT until unsorted section is empty
		FIND smallest (minimum) element in unsorted section
		SWAP with the current element
		ADVANCE to next element
	ENDREPEAT
	*/

	for (int i = 0; i < count - 2; i++)
	{
		int index = FindMinimum(&numbers[i + 1], count - i - 1);
		index += i + 1;

		Swap(numbers[index], numbers[i]);
	}
}

void Merge(int* numbers, int start, int mid, int end)
{
	// Combine two subarrays into a temporary array, sorting as we go.
	// Then copy back into our original array.

	int* temp = new int[end - start + 1];

	// Index into temp
	int i = 0;

	// Index into start -> mid
	int left = start;

	// Index into mid+1 -> end
	int right = mid + 1;


	// Compare the leading elements of left and right sides.  Copy the one that is lesser.
	while (left <= mid && right <= end)
	{
		if (numbers[left] <= numbers[right])
		{
			temp[i] = numbers[left];
			++left;
		}
		else
		{
			temp[i] = numbers[right];
			++right;
		}
		++i;
	}


	// Take any remaining elements from left side and append them to temp
	while (left <= mid)
	{
		temp[i] = numbers[left];
		++i;
		++left;
	}

	while (right <= end)
	{
		temp[i] = numbers[right];
		++i;
		++right;
	}

	// Copy back into result
	/*for (int j = 0; j < end - start + 1; ++j)
	{
		numbers[start + j] = temp[j];
	}*/
	memcpy(numbers + start, temp, (end - start + 1) * sizeof(int));
}
// start   mid    mid+1  end             
// [0,     1,  |   2,     3]

void MergeSortRecursive(int* numbers, int start, int end)
{

	/*
	Division step
	Merge step


	Termination case:
	Array size of 1 or less
	Something like: if(end - start <= 0)

	Division:
	count = end - start + 1;
	mid = start + count/2 -> (start + end)/2
	// Left side: numbers[start] to numbers[mid]
	// Right side: numbers[mid+1] to numbers[end]
	MergeSortRecursive(numbers, start, mid)
	MergeSortRecursive(number, mid+1, end)

	Merge:
	// Sort leading elements of the two subarrays into a new temporary array
	// Then copy that temporary array back onto the (start to end) part of the numbers array
	Merge(numbers, start, mid, end);


	*/


	if (end - start <= 0)
		return;

	int count = end - start + 1;
	int mid = start + (count - 1) / 2;  // Make sure the subarrays get split evenly (mid is an index)

	MergeSortRecursive(numbers, start, mid);
	MergeSortRecursive(numbers, mid + 1, end);


	Merge(numbers, start, mid, end);
}

void MergeSort(int numbers[], int count)
{
	MergeSortRecursive(numbers, 0, count - 1);
}

int Partition(int* numbers, int first, int last)
{
	int pivot = numbers[last];//arbitrarily choosing the last index as the pivot
	int leftPointer = (first - 1);//left pointer starts at -1 the first time and thats fine because we're incrementing it before we use it

	for (int i = first; i < last; ++i)
	{
		if (numbers[i] < pivot)//the number compared to the pivot is less than the pivot.
		{
			Swap(numbers[i], numbers[++leftPointer]);//swap said index with our left pointer and move the left pointer to live data
		}
	}
	Swap(numbers[leftPointer + 1], numbers[last]);//after all comparisons we swap our left pointer with our pivot.
	return (leftPointer + 1);//return the left pointer's index to be used as the pivot in the next recursion
}

void QuickSortRecursive(int* numbers, int first, int last)
{
	if (first < last)//check if recursion should end
	{
		int newPivot = Partition(numbers, first, last);//this returns the left pointer's index from the partitioned collection when it is done sorting
		QuickSortRecursive(numbers, first, newPivot - 1);//decrementing last will eventually cause it to be less than first
		QuickSortRecursive(numbers, newPivot + 1, last);//incrementing first will eventually cause it to be more than last
	}
}
void QuickSort(int numbers[], int count)
{
	QuickSortRecursive(numbers, 0, count - 1);
}

void DoSort(std::function<void(int[], int)> SortAlgo, string name)
{
	int numbers[] = { 5, 2, 42, 55, 31, 42, 13, -1, 0, 3,1,2 };

	SortAlgo(numbers, 12);

	cout << name << ":" << endl;
	for (int i = 0; i < 12; ++i)
	{
		cout << numbers[i] << " ";
	}
	cout << endl;
}

void TestSelectionSortAverage()
{
	ofstream myfile;
	myfile.open("averageselectionsort.txt");
	myfile << "# N" << "\t" << "Nanoseconds" << "\n";
	for (int i = 1; i <= 1000; i += 10)
	{
		int* temp = new int[i];
		for (int j = i, q = 0; j >= 0; j--, q++)
		{
			temp[q] = rand() % 10;
		}
		high_resolution_clock::time_point start = high_resolution_clock::now();

		SelectionSort(temp, i);

		high_resolution_clock::time_point finish = high_resolution_clock::now();
		nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);
		myfile << i << "\t" << totalNS.count() << "\n";
		temp = nullptr;
		delete[] temp;
	}
	myfile.close();
}

void TestSelectionSortWorst()
{
	ofstream myfile;
	myfile.open("worstselectionsort.txt");
	myfile << "# N" << "\t" << "Nanoseconds" << "\n";
	for (int i = 1; i <= 1000; i += 10)
	{
		int* temp = new int[i];
		for (int j = i, q = 0; j >= 0; j--, q++)
		{
			temp[q] = j;
		}
		high_resolution_clock::time_point start = high_resolution_clock::now();

		SelectionSort(temp, i);

		high_resolution_clock::time_point finish = high_resolution_clock::now();
		nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);
		myfile << i << "\t" << totalNS.count() << "\n";
		temp = nullptr;
		delete[] temp;
	}
	myfile.close();
}

void TestMergeSortAverage()
{
	ofstream myfile;
	myfile.open("averagemergesort.txt");
	myfile << "# N" << "\t" << "Nanoseconds" << "\n";
	for (int i = 1; i <= 1000; i += 10)
	{
		int* temp = new int[i];
		for (int j = i, q = 0; j >= 0; j--, q++)
		{
			temp[q] = rand() % 10;
		}
		high_resolution_clock::time_point start = high_resolution_clock::now();

		MergeSort(temp, i);

		high_resolution_clock::time_point finish = high_resolution_clock::now();
		nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);
		myfile << i << "\t" << totalNS.count() << "\n";
		temp = nullptr;
		delete[] temp;
	}
	myfile.close();
}
void TestMergeSortWorst()
{
	ofstream myfile;
	myfile.open("worstmergesort.txt");
	myfile << "# N" << "\t" << "Nanoseconds" << "\n";
	for (int i = 1; i <= 1000; i += 10)
	{
		int* temp = new int[i];
		for (int j = i, q = 0; j >= 0; j--, q++)
		{
			temp[q] = j;
		}
		high_resolution_clock::time_point start = high_resolution_clock::now();

		MergeSort(temp, i);

		high_resolution_clock::time_point finish = high_resolution_clock::now();
		nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);
		myfile << i << "\t" << totalNS.count() << "\n";
		temp = nullptr;
		delete[] temp;
	}
	myfile.close();
}

void TestQuickSortWorst()
{
	ofstream myfile;
	myfile.open("worstquicksort.txt");
	myfile << "# N" << "\t" << "Nanoseconds" << "\n";
	for (int i = 1; i <= 1000; i += 10)
	{
		int* temp = new int[i];
		for (int j = i, q = 0; j >= 0; j--, q++)
		{
			temp[q] = j;
		}
		high_resolution_clock::time_point start = high_resolution_clock::now();

		QuickSort(temp, i);

		high_resolution_clock::time_point finish = high_resolution_clock::now();
		nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);
		myfile << i << "\t" << totalNS.count() << "\n";
		temp = nullptr;
		delete[] temp;
	}
	myfile.close();
}

void TestQuickSortAverage()
{
	ofstream myfile;
	myfile.open("averagequicksort.txt");
	myfile << "# N" << "\t" << "Nanoseconds" << "\n";
	for (int i = 1; i <= 1000; i += 10)
	{
		int* test = new int[i];
		for (int j = 0; j < i; j++)
		{
			test[j] = rand() % 10;
		}
		high_resolution_clock::time_point start = high_resolution_clock::now();

		QuickSort(test, i);

		high_resolution_clock::time_point finish = high_resolution_clock::now();
		nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);
		myfile << i << "\t" << totalNS.count() << "\n";
		test = nullptr;
		delete[] test;
	}
	myfile.close();
}


int main(int argc, char* argv[])
{

	/*DoSort(BubbleSort, "Bubble Sort");

	DoSort(InsertionSort, "Insertion Sort");

	DoSort(SelectionSort, "Selection Sort");*/

	DoSort(QuickSort, "QuickSort");

	//TestSelectionSortWorst();
	//TestSelectionSortAverage();

	//TestMergeSortAverage();
	//TestMergeSortWorst();

	//TestQuickSortAverage();
	//TestQuickSortWorst();

	//system("gnuplot averagecase.txt");
	//system("gnuplot worstcase.txt");

	//system("gnuplot quicksort.txt");
	system("pause");
	return 0;
}